# Testing Virtuoso Open Source

```
$ docker compose -f docker/docker-compose.yml up
```

## Login

- Visit <http://localhost:8890/conductor>
- Login with user: "dba", password: "pass"
- Profit!

## Data

- Export some data as `xml/rdf`
- Go to **Linked Data** > **Quad Store Upload** in the UI and upload the file to the quad store there
- Go to **Linked Data** > **SPARQL** to test queries against the triple data
